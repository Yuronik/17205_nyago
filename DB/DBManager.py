import pandas as pd
from sqlalchemy.orm import lazyload, joinedload

from DB_Creator import DB_creator


class DBManager:
    creator = DB_creator()

    @staticmethod
    def show_competitions():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Competition.competition_name, DBManager.creator.Competition.competition_date, DBManager.creator.Facility.facility_name,
            DBManager.creator.Organizer.organizer_name, DBManager.creator.Sport_type.type_name).join(
            (DBManager.creator.Facility, DBManager.creator.Competition.facility_id == DBManager.creator.Facility.facility_id),
            (DBManager.creator.Organizer, DBManager.creator.Competition.organizer_id == DBManager.creator.Organizer.organizer_id),
            (DBManager.creator.Sport_type, DBManager.creator.Competition.sport_type_id == DBManager.creator.Sport_type.sport_type_id)))

    @staticmethod
    def show_organizers():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Organizer.organizer_id, DBManager.creator.Organizer.organizer_name))

    @staticmethod
    def show_sport_types():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Sport_type.sport_type_id,
                                                                       DBManager.creator.Sport_type.type_name))

    @staticmethod
    def show_trainers():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Trainer.trainer_id,
                                                                       DBManager.creator.Trainer.full_name))

    @staticmethod
    def show_sport_clubs():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Sport_club.club_id,
                                                                       DBManager.creator.Sport_club.club_name))

    @staticmethod
    def show_ranks():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Rank.rank_id,
                                                                       DBManager.creator.Rank.rank_name, DBManager.creator.Rank.weight))

    @staticmethod
    def show_facilities():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Facility.facility_id,
                                                                       DBManager.creator.Facility.facility_name,
                                                                       DBManager.creator.Facility.court_id,
                                                                       DBManager.creator.Facility.stadium_id))


    @staticmethod
    def show_competitors():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Sportsmen_in_competitions.id,
                                                                       DBManager.creator.Sportsmen_in_competitions.competition_id, DBManager.creator.Competition.competition_name,
                                                                       DBManager.creator.Sportsmen_in_competitions.sportsman_id, DBManager.creator.Sportsman.full_name).join(
            (DBManager.creator.Competition, DBManager.creator.Sportsmen_in_competitions.competition_id == DBManager.creator.Competition.competition_id),
            (DBManager.creator.Sportsman, DBManager.creator.Sportsmen_in_competitions.sportsman_id == DBManager.creator.Sportsman.sportsman_id)
        ))

    @staticmethod
    def show_awards():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Award.award_id,
                                                                       DBManager.creator.Award.category,
                                                                       DBManager.creator.Award.competition_id,
                                                                       DBManager.creator.Award.the_first_place_id,
                                                                       DBManager.creator.Award.the_second_place_id,
                                                                       DBManager.creator.Award.the_third_place_id))


    @staticmethod
    def show_sportsmen():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Sportsman.sportsman_id,
                                       DBManager.creator.Sportsman.full_name, DBManager.creator.Sportsman.rank_id, DBManager.creator.Sportsman.sport_club_id))

    @staticmethod
    def show_trainer_by_sport_type(sport_type):
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Trainer.full_name, DBManager.creator.Sport_type.type_name).join(
            (DBManager.creator.Trainers_doing_sport_types, DBManager.creator.Trainers_doing_sport_types.trainer_id == DBManager.creator.Trainer.trainer_id),
            (DBManager.creator.Sport_type, DBManager.creator.Trainers_doing_sport_types.sport_type_id == DBManager.creator.Sport_type.sport_type_id)
        ).filter(DBManager.creator.Sport_type.sport_type_id == sport_type.sport_type_id))

    @staticmethod
    def show_sportsmen_trainers(sportsman):
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Sportsman.full_name, DBManager.creator.Trainer.full_name).join(
            (DBManager.creator.Sportsmen_at_trainers, DBManager.creator.Sportsmen_at_trainers.sportsman_id == DBManager.creator.Sportsman.sportsman_id),
            (DBManager.creator.Trainer, DBManager.creator.Sportsmen_at_trainers.trainer_id == DBManager.creator.Trainer.trainer_id)
        ).filter(DBManager.creator.Sportsman.sportsman_id == sportsman.sportsman_id))

    @staticmethod
    def select_sportsman_at_trainers():
        return DBManager.as_data_frame(DBManager.creator.session.query(DBManager.creator.Sportsmen_at_trainers.id, DBManager.creator.Sportsmen_at_trainers.sportsman_id, DBManager.creator.Sportsmen_at_trainers.trainer_id))



    @staticmethod
    def get_table_names():
        return ["Organizer", "Rank", "Sport type", "Trainer", "Sport club", "Sportsman", "Facility", "Competition", "Competitor", "Award"]

    @staticmethod
    def select_facility_types():
        return ['Court', 'Stadium']



    @staticmethod
    def select_facilities():
        return DBManager.creator.session.query(DBManager.creator.Facility).all()
    @staticmethod
    def select_organizers():
        return DBManager.creator.session.query(DBManager.creator.Organizer).all()

    @staticmethod
    def select_competitions():
        return DBManager.creator.session.query(DBManager.creator.Competition).all()

    @staticmethod
    def select_ranks():
        return DBManager.creator.session.query(DBManager.creator.Rank).all()

    @staticmethod
    def select_sport_types():
        return DBManager.creator.session.query(DBManager.creator.Sport_type).all()

    @staticmethod
    def select_sport_clubs():
        return DBManager.creator.session.query(DBManager.creator.Sport_club).all()

    @staticmethod
    def select_trainers():
        return DBManager.creator.session.query(DBManager.creator.Trainer).all()

    @staticmethod
    def select_sportsman():
        return DBManager.creator.session.query(DBManager.creator.Sportsman).all()



    @staticmethod
    def insert_organizer(name):
        DBManager.creator.session.add(DBManager.creator.Organizer(organizer_name=name))
        DBManager.creator.session.commit()

    @staticmethod
    def insert_sport_type(type_name):
        sport_type = DBManager.creator.Sport_type(type_name=type_name)
        DBManager.creator.session.add(sport_type)
        DBManager.creator.session.commit()

    @staticmethod
    def insert_trainer(full_name, sport_type):
        trainer = DBManager.creator.Trainer(full_name=full_name)

        t_d_s_t = DBManager.creator.Trainers_doing_sport_types()
        t_d_s_t.trainer = trainer
        t_d_s_t.sport_type = sport_type
        DBManager.creator.session.add(t_d_s_t)

        trainer.sport_types.append(t_d_s_t)
        DBManager.creator.session.add(trainer)
        sport_type.trainers.append(t_d_s_t)
        DBManager.creator.session.commit()

    @staticmethod
    def insert_sport_club(club_name):
        DBManager.creator.session.add(DBManager.creator.Sport_club(club_name=club_name))
        DBManager.creator.session.commit()

    @staticmethod
    def insert_rank(rank_name, rank_weight):
        rank = DBManager.creator.Rank(rank_name=rank_name, weight=rank_weight)
        DBManager.creator.session.add(rank)
        DBManager.creator.session.commit()

    @staticmethod
    def insert_sportsman(full_name, rank, sport_club, trainer, sport_type):
        sportsman = DBManager.creator.Sportsman(full_name=full_name)
        sportsman.rank = rank
        sportsman.sport_club = sport_club


        s_at_tr = DBManager.creator.Sportsmen_at_trainers()
        s_at_tr.sportsman = sportsman
        s_at_tr.trainer = trainer

        sportsman.trainers.append(s_at_tr)
        
        s_d_s_t = DBManager.creator.Sportsmen_doing_sport_types()
        s_d_s_t.sport_type = sport_type
        s_d_s_t.sportsman = sportsman

        DBManager.creator.session.add(s_at_tr)
        DBManager.creator.session.add(s_d_s_t)

        trainer.sportsmen.append(s_at_tr)

        sport_type.sportsmen.append(s_d_s_t)
        sportsman.sport_types.append(s_d_s_t)

        DBManager.creator.session.add(sportsman)

        DBManager.creator.session.commit()

    @staticmethod
    def insert_facility(facility_name, property, type):
        facility = DBManager.creator.Facility(facility_name=facility_name)
        if type == 'Court':
            court = DBManager.creator.Court(coating_type=property)
            facility.court = court
            facility.stadium = None
            DBManager.creator.session.add(court)

        if type == 'Stadium':
            stadium = DBManager.creator.Stadium(capacity=property)
            facility.stadium = stadium
            facility.court = None
            DBManager.creator.session.add(stadium)
        DBManager.creator.session.add(facility)
        DBManager.creator.session.commit()


    @staticmethod
    def insert_competition(competition_name, competition_date, facility, organizer, sport_type):
        competition = DBManager.creator.Competition(competition_name=competition_name, competition_date=competition_date)
        competition.organizer = organizer
        competition.facility = facility
        competition.sport_type = sport_type
        DBManager.creator.session.add(competition)
        DBManager.creator.session.commit()

    @staticmethod
    def insert_competitor(competition, sportsman):
        competitor = DBManager.creator.Sportsmen_in_competitions()

        competitor.sportsman = sportsman
        competitor.competition = competition
        DBManager.creator.session.add(competitor)

        sportsman.competitions.append(competitor)
        competition.sportsmen.append(competitor)
        DBManager.creator.session.commit()

    @staticmethod
    def insert_award(category, competition, first_place, second_place, third_place):
        award = DBManager.creator.Award(category=category)
        award.competition = competition
        award.the_first_place = first_place
        award.the_second_place = second_place
        award.the_third_place = third_place
        DBManager.creator.session.add(award)

        competition.awards.append(award)

        DBManager.creator.session.commit()


    @staticmethod
    def as_data_frame(query):
        if (0 != len(query.all())):
            result = [[instance[i] for i in range(len(instance))] for instance in query.all()]
            fields = [name for name in query[0]._fields]
            return pd.DataFrame(result, columns=fields)
        else:
            return None

    @staticmethod
    def as_list(query):
        return [[instance[i] for i in range(len(instance))] for instance in query.all()]


