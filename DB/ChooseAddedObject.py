from PyQt5.QtWidgets import QDialog, QGridLayout, QComboBox, QPushButton

from CompetitionAddDialog import CompetitionAddDialog
from CompetitorAddDialog import CompetitorAddDialog
from DBManager import DBManager
from FacilityAddDialog import FacilityAddDialog
from OrganizerAddDialog import OrganizerAddDialog

from RankAddDialog import RankAddDialog
from SportClubAddDialog import SportClubAddDialog
from SportTypeAddDialog import SportTypeAddDialog
from SportsmanAddDialog import SportsmanAddDialog
from TrainerAddDialog import TrainerAddDialog
from AwardAddedDialog import AwardAddedDialog

class ChooseAddedObjDialog(QDialog):
    drop_down_list = None
    main_window = None

    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window
        self.init_added_obj_dialog()

    def init_added_obj_dialog(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        self.drop_down_list = QComboBox()
        self.drop_down_list.addItems(DBManager.get_table_names())
        grid_layout.addWidget(self.drop_down_list)

        button_ok = QPushButton("OK")
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def button_ok_clicked(self):
        self.close()
        text = str(self.drop_down_list.currentText())
        if "Organizer" == text:
            OrganizerAddDialog()
        if "Sport type" == text:
            SportTypeAddDialog()
        if "Trainer" == text:
            TrainerAddDialog()
        if "Sport club" == text:
            SportClubAddDialog()
        if "Rank" == text:
            RankAddDialog()
        if "Sportsman" == text:
            SportsmanAddDialog()
        if "Facility" == text:
            FacilityAddDialog()
        if "Competition" == text:
            CompetitionAddDialog()
        if "Competitor" == text:
            CompetitorAddDialog()
        if "Award" == text:
            AwardAddedDialog()

        self.main_window.init_competition_table()

