from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QComboBox, QPushButton, QLabel
from DBManager import DBManager


class FacilityAddDialog(QDialog):
    facility_name = None
    type_list = None
    grid_layout = None
    property = None
    def __init__(self):
        super().__init__()
        self.init_facility_add_form()

    def init_facility_add_form(self):
        self.grid_layout = QGridLayout()
        self.setLayout(self.grid_layout)

        label = QLabel()
        label.setText('Facility name')
        self.grid_layout.addWidget(label)

        facility_name_line = QLineEdit()
        facility_name_line.textChanged.connect(self.get_facility_name)
        self.grid_layout.addWidget(facility_name_line)

        label = QLabel()
        label.setText('Facility type')
        self.grid_layout.addWidget(label)

        self.type_list = QComboBox()
        types = DBManager.select_facility_types()
        self.type_list.addItems(types)
        self.type_list.currentTextChanged.connect(self.load_qline)
        self.grid_layout.addWidget(self.type_list)

        self.property_label = QLabel()
        self.property_label.setText('Coating type')
        self.grid_layout.addWidget(self.property_label)

        property_line = QLineEdit()
        property_line.textChanged.connect(self.get_property)
        self.grid_layout.addWidget(property_line)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        self.grid_layout.addWidget(button_ok)

        self.exec()

    def get_facility_name(self, text):
        self.facility_name = text

    def load_qline(self, text):
        if text == 'Court':
            self.property_label.setText('Coating type')
        if text == 'Stadium':
            self.property_label.setText('capacity')


    def get_property(self, text):
        self.property = text

    def button_ok_clicked(self):
 #       if self.rank_name is not None and "" != self.rank_name and self.rank_weight is not None:
        self.close()
#        rank = self.rank_list.currentData()
#        sport_club = self.sport_club_list.currentData()
#        trainer = self.trainer_list.currentData()
#        sport_type = self.sport_type_list.currentData()
        DBManager.insert_facility(self.facility_name, self.property, self.type_list.currentText())

