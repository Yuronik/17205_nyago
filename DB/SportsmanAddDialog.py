from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QComboBox, QPushButton, QLabel

from DBManager import DBManager


class SportsmanAddDialog(QDialog):
    full_name = None
    rank_list = None
    sport_club_list = None
    trainer_list = None
    sport_type_list = None

    def __init__(self):
        super().__init__()
        self.init_sportsman_add_form()

    def init_sportsman_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Sportsman name')
        grid_layout.addWidget(label)

        fullNameLine = QLineEdit()
        fullNameLine.textChanged.connect(self.get_full_name)
        grid_layout.addWidget(fullNameLine)

        label = QLabel()
        label.setText('Rank')
        grid_layout.addWidget(label)

        self.rank_list = QComboBox()
        ranks = DBManager.select_ranks()
        for rank in ranks:
            self.rank_list.addItem(rank.rank_name, rank)
        grid_layout.addWidget(self.rank_list)

        label = QLabel()
        label.setText('Sport club')
        grid_layout.addWidget(label)

        self.sport_club_list = QComboBox()
        clubs = DBManager.select_sport_clubs()
        for club in clubs:
            self.sport_club_list.addItem(club.club_name, club)
        grid_layout.addWidget(self.sport_club_list)

        label = QLabel()
        label.setText('Trainer')
        grid_layout.addWidget(label)

        self.trainer_list = QComboBox()
        trainers = DBManager.select_trainers()
        for trainer in trainers:
            self.trainer_list.addItem(trainer.full_name, trainer)
        grid_layout.addWidget(self.trainer_list)

        label = QLabel()
        label.setText('Sport type')
        grid_layout.addWidget(label)

        self.sport_type_list = QComboBox()
        sport_types = DBManager.select_sport_types()
        for sport_type in sport_types:
            self.sport_type_list.addItem(sport_type.type_name, sport_type)
        grid_layout.addWidget(self.sport_type_list)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def get_full_name(self, text):
        self.full_name = text

    def button_ok_clicked(self):
 #       if self.rank_name is not None and "" != self.rank_name and self.rank_weight is not None:
        self.close()
        rank = self.rank_list.currentData()
        sport_club = self.sport_club_list.currentData()
        trainer = self.trainer_list.currentData()
        sport_type = self.sport_type_list.currentData()
        DBManager.insert_sportsman(self.full_name, rank, sport_club, trainer, sport_type)

