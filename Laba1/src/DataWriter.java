import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

public class DataWriter implements IDataWriter {

    @Override
    public void csvOutput(Writer writer, List<Map.Entry<String, Integer>> list, int countWords) throws IOException {
        writer.append("Word;Frequency;Frequency %\n");
        for (Map.Entry<String, Integer> entry : list) {
            writer.append(entry.getKey()).append(";").append(entry.getValue().toString()).append(";").append(Float.toString(100.f* entry.getValue() /  countWords)).append("\n");
        }

    }
}
