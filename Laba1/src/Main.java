import java.io.*;


public class Main {
    public static void main(String[] args) {
        Tokenizer tokenizer = new Tokenizer();
        Aggregator aggregator = new Aggregator();
        String currentWord;
        try (Reader reader = new BufferedReader(new FileReader(new File(args[0])))) {
            while ((currentWord = tokenizer.nextToken(reader)).length() != 0) {
                //currentWord = tokenizer.nextToken(reader);
                //if (currentWord.length() != 0)
                    aggregator.addToken(currentWord);
            }
            Writer writer = new FileWriter("test.csv");
            DataWriter dataWriter = new DataWriter();
            dataWriter.csvOutput(writer, aggregator.sortMap(), aggregator.getCountWords());
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}