from PyQt5.QtWidgets import QWidget, QGridLayout, QMainWindow, QPushButton, QTableView

from ChooseAddedObject import ChooseAddedObjDialog
from ChooseShowingObject import ChooseShowingObjDialog
from DBManager import DBManager
from TableModel import TableModel


class MainWindow(QMainWindow):
    query = None
    params = None
    def __init__(self, query):
        super().__init__()
        self.query = query
        self.init_competition_table()

    def init_competition_table(self):
        self.setGeometry(100, 100, 800, 800)
        self.setWindowTitle('Sport competitions')

        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        grid_layout = QGridLayout()
        central_widget.setLayout(grid_layout)

        if self.params is None:
            data = self.query.__call__()
        else:
            data = self.query.__call__(self.params[0])
        if data is not None:
            table = QTableView()
            model = TableModel(data)
            table.setModel(model)
            grid_layout.addWidget(table)

        button_add = QPushButton("Add", self)
        button_add.clicked.connect(self.button_add_clicked)
        grid_layout.addWidget(button_add)

        button_info = QPushButton("Info", self)
        button_info.clicked.connect(self.button_info_clicked)
        grid_layout.addWidget(button_info)

        button_delete = QPushButton("Delete", self)
        button_delete.clicked.connect(self.button_delete_clicked)
        grid_layout.addWidget(button_delete)

        self.show()

    def button_add_clicked(self):
        ChooseAddedObjDialog(self)

    def button_info_clicked(self):
        ChooseShowingObjDialog(self)

    def button_delete_clicked(self):
        sender = self.sender()
        self.statusBar().showMessage(sender.text() + ' was pressed')









