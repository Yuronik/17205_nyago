import sys

from PyQt5.QtWidgets import QApplication

from DBManager import DBManager
from MainWindow import MainWindow

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = MainWindow(DBManager.show_competitions)
    sys.exit(app.exec_())



