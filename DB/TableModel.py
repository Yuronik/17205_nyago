from PyQt5.QtCore import QAbstractTableModel, Qt


class TableModel(QAbstractTableModel):
    def __init__(self, data):
        super(TableModel, self).__init__()
        self._data = data
#        self.columns = list(self._data[0]._fields)

    def data(self, index, role):
        if role == Qt.DisplayRole:
            value = self._data.iloc[index.row(), index.column()]
            return str(value)

    def rowCount(self, index):
        # The length of the outer list.
        return self._data.shape[0]

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return self._data.shape[1]

    def headerData(self, section, orientation, role):
        # section is the index of the column/row.
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return str(self._data.columns[section])

'''
    def insertRows(self, data) -> bool:
        row_count = len(self._data)
        self.beginInsertRows(self, QModelIndex(), row_count, row_count)
#        insert new rows to the underlying data
        self._data.append(data)
        row_count += 1
        self.endInsertRows()
        return True

    def modelReset(self) -> None:
        self.beginResetModel();
        query = DBManager.showing_query()
        result = [[instance[i] for i in range(len(instance))] for instance in query.all()]
        fields = [name for name in query[0]._fields]
        _data = pd.DataFrame(result, columns=fields)
        self.endResetModel();
'''

