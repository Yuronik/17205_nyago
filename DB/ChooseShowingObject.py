from PyQt5.QtWidgets import QDialog, QGridLayout, QComboBox, QPushButton

from ChooseSportTypeDialog import ChooseSportTypeDialog
from ChooseTrainerDialog import ChooseTrainerDialog
from CompetitionAddDialog import CompetitionAddDialog
from CompetitorAddDialog import CompetitorAddDialog
from DBManager import DBManager
from FacilityAddDialog import FacilityAddDialog
from OrganizerAddDialog import OrganizerAddDialog

from RankAddDialog import RankAddDialog
from SportClubAddDialog import SportClubAddDialog
from SportTypeAddDialog import SportTypeAddDialog
from SportsmanAddDialog import SportsmanAddDialog
from TrainerAddDialog import TrainerAddDialog
from AwardAddedDialog import AwardAddedDialog

class ChooseShowingObjDialog(QDialog):
    drop_down_list = None
    main_window = None

    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window
        self.init_showing_obj_dialog()

    def init_showing_obj_dialog(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        self.drop_down_list = QComboBox()
        self.drop_down_list.addItems(['Trainers by sport type', "Sportsman's trainers"])
        self.drop_down_list.addItems(DBManager.get_table_names())
        grid_layout.addWidget(self.drop_down_list)

        button_ok = QPushButton("OK")
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def button_ok_clicked(self):
        self.close()
        text = str(self.drop_down_list.currentText())
        if "Organizer" == text:
            query = DBManager.show_organizers
            self.main_window.params = None
        if "Competition" == text:
            query = DBManager.show_competitions
            self.main_window.params = None
        if "Sport type" == text:
            query = DBManager.show_sport_types
            self.main_window.params = None
        if "Trainer" == text:
            query = DBManager.show_trainers
            self.main_window.params = None
        if "Sport club" == text:
            query = DBManager.show_sport_clubs
            self.main_window.params = None
        if "Rank" == text:
            query = DBManager.show_ranks
            self.main_window.params = None
        if "Sportsman" == text:
            query = DBManager.show_sportsmen
            self.main_window.params = None
        if "Facility" == text:
            query = DBManager.show_facilities
            self.main_window.params = None
        if "Competitor" == text:
            query = DBManager.show_competitors
            self.main_window.params = None
        if "Award" == text:
            query = DBManager.show_awards
            self.main_window.params = None
        if "Trainers by sport type" == text:
            ChooseSportTypeDialog(self.main_window)
            query = self.main_window.query
        if "Sportsman's trainers" == text:
            ChooseTrainerDialog(self.main_window)
            query = self.main_window.query

        self.main_window.query = query
        self.main_window.init_competition_table()

