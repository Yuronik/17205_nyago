package tests.src.test.java;

import org.junit.Assert;
import org.junit.Test;
import ru.my.operations.MinusOperation;
import ru.my.source.Context;
import ru.my.source.Parser;


public class MinusOperationTest {

    @Test
    public void doAction() {
        Context context = new Context();
        MinusOperation div = new MinusOperation();
        Parser parser = new Parser();
        String line = "-";
        double value1 = 1;
        double value2 = 2;
        context.push(value1);
        context.push(value2);
        div.doAction(context, parser.parseLine(line));
        Assert.assertEquals(value2-value1, context.pop(), 0.0001);

    }
}