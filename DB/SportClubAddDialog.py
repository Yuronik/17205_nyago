from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QPushButton, QLabel

from DBManager import DBManager


class SportClubAddDialog(QDialog):
    club_name = None

    def __init__(self):
        super().__init__()
        self.init_sport_club_add_form()

    def init_sport_club_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Sport club name')
        grid_layout.addWidget(label)

        qLine = QLineEdit()
        qLine.textChanged.connect(self.get_text)
        grid_layout.addWidget(qLine)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def get_text(self, text):
        self.club_name = text

    def button_ok_clicked(self):
        if self.club_name is not None and "" != self.club_name:
            self.close()
            DBManager.insert_sport_club(self.club_name)

