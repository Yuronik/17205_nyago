from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QComboBox, QPushButton, QLabel, QDateEdit
from DBManager import DBManager
from ErrorDialog import ErrorDialog


class CompetitionAddDialog(QDialog):
    competition_name = None
    competition_date = QDate(2000, 1, 1)
    facility_list = None
    organizer_list = None
    sport_type_list = None

    def __init__(self):
        super().__init__()
        self.init_competition_add_form()

    def init_competition_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Competition name')
        grid_layout.addWidget(label)

        competition_name_line = QLineEdit()
        competition_name_line.textChanged.connect(self.get_competition_name)
        grid_layout.addWidget(competition_name_line)

        label = QLabel()
        label.setText('Competition date')
        grid_layout.addWidget(label)

        competition_date_line = QDateEdit()
        competition_date_line.dateChanged.connect(self.get_competition_date)
        grid_layout.addWidget(competition_date_line)

        label = QLabel()
        label.setText('Facility')
        grid_layout.addWidget(label)

        self.facility_list = QComboBox()
        facilities = DBManager.select_facilities()
        for facility in facilities:
            self.facility_list.addItem(facility.facility_name, facility)
        grid_layout.addWidget(self.facility_list)

        label = QLabel()
        label.setText('Organizer')
        grid_layout.addWidget(label)

        self.organizer_list = QComboBox()
        organizers = DBManager.select_organizers()
        for organizer in organizers:
            self.organizer_list.addItem(organizer.organizer_name, organizer)
        grid_layout.addWidget(self.organizer_list)

        label = QLabel()
        label.setText('Sport type')
        grid_layout.addWidget(label)

        self.sport_type_list = QComboBox()
        sport_types = DBManager.select_sport_types()
        for sport_type in sport_types:
            self.sport_type_list.addItem(sport_type.type_name, sport_type)
        grid_layout.addWidget(self.sport_type_list)


        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def get_competition_name(self, text):
        self.competition_name = text

    def get_competition_date(self, date):
        self.competition_date = date

    def button_ok_clicked(self):
        if self.competition_name is not None and "" != self.competition_name:
            self.close()
            facility = self.facility_list.currentData()
            organizer = self.organizer_list.currentData()
            sport_type = self.sport_type_list.currentData()
            DBManager.insert_competition(self.competition_name, self.competition_date.toPyDate(), facility, organizer, sport_type)
        else:
            ErrorDialog()



