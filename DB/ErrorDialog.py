from PyQt5.QtWidgets import QDialog, QGridLayout, QLabel, QPushButton


class ErrorDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.init_error_form()

    def init_error_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Enter the name of the competition!')
        grid_layout.addWidget(label)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def button_ok_clicked(self):
        self.close()