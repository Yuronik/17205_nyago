from datetime import datetime

from PyQt5.QtCore import QDate

from DBManager import DBManager


def add_all():
    #add ranks
    mc = DBManager.creator.Rank(rank_name='МС', weight=8)
    DBManager.creator.session.add(mc)

    DBManager.creator.session.add_all([
        DBManager.creator.Rank(rank_name='третий юношеский.', weight=1),
        DBManager.creator.Rank(rank_name='второй юношеский', weight=2),
        DBManager.creator.Rank(rank_name='первый юношеский', weight=3),
        DBManager.creator.Rank(rank_name='третий спортивный разряд', weight=4),
        DBManager.creator.Rank(rank_name='второй спортивный разряд', weight=5),
        DBManager.creator.Rank(rank_name='первый спортивный разряд', weight=6),
        DBManager.creator.Rank(rank_name='КМС', weight=7),
        DBManager.creator.Rank(rank_name='МСМК', weight=9),
        DBManager.creator.Rank(rank_name='ЗМС', weight=10)
    ])
    #add sport types
    football = DBManager.creator.Sport_type(type_name='Футбол')
    DBManager.creator.session.add(football)
    tx = DBManager.creator.Sport_type(type_name='Тхэквондо')
    DBManager.creator.session.add(tx)

    DBManager.creator.session.add_all([
        DBManager.creator.Sport_type(type_name='Волейбол'),
        DBManager.creator.Sport_type(type_name='Баскетбол'),
        DBManager.creator.Sport_type(type_name='Бокс'),
        DBManager.creator.Sport_type(type_name='Легкая атлетика'),
        DBManager.creator.Sport_type(type_name='Гребля'),
        DBManager.creator.Sport_type(type_name='Каратэ'),
    ])

    #add organizers
    organizer = DBManager.creator.Organizer(organizer_name='Клуб смешанных единоборств Олимп')
    DBManager.creator.session.add(organizer)
    DBManager.creator.session.add_all([
        DBManager.creator.Organizer(organizer_name='ДЮСШ 110'),
        DBManager.creator.Organizer(organizer_name='Бойцовский клуб'),
        DBManager.creator.Organizer(organizer_name='Золотая нога'),
    ])

    #add facility
    facility_1 = DBManager.creator.Facility(facility_name='Корт южный')
    court = DBManager.creator.Court(coating_type='Резина')
    facility_1.court = court
    facility_1.stadium = None
    DBManager.creator.session.add(court)
    DBManager.creator.session.add(facility_1)

    facility_2 = DBManager.creator.Facility(facility_name='Олимпийский')
    stadium = DBManager.creator.Stadium(capacity=10000)
    facility_2.stadium = stadium
    facility_2.court = None
    DBManager.creator.session.add(stadium)
    DBManager.creator.session.add(facility_2)

    #add sport_club
    sport_club_1 = DBManager.creator.Sport_club(club_name='Муссон')
    sport_club_2 = DBManager.creator.Sport_club(club_name='Авангард')
    DBManager.creator.session.add_all([sport_club_1, sport_club_2])

    #add trainers
    trainer_1 = DBManager.creator.Trainer(full_name='Петров Федр Иосифович')
    t_d_s_t_1 = DBManager.creator.Trainers_doing_sport_types()
    t_d_s_t_1.trainer = trainer_1
    t_d_s_t_1.sport_type = football
    DBManager.creator.session.add(t_d_s_t_1)

    trainer_1.sport_types.append(t_d_s_t_1)
    DBManager.creator.session.add(trainer_1)
    football.trainers.append(t_d_s_t_1)

    trainer_2 = DBManager.creator.Trainer(full_name='Поляков Александр Иванович')
    t_d_s_t_2 = DBManager.creator.Trainers_doing_sport_types()
    t_d_s_t_2.trainer = trainer_2
    t_d_s_t_2.sport_type = tx
    DBManager.creator.session.add(t_d_s_t_2)

    trainer_1.sport_types.append(t_d_s_t_2)
    DBManager.creator.session.add(trainer_2)
    tx.trainers.append(t_d_s_t_2)

    #add sportsmen
    sportsman_1 = DBManager.creator.Sportsman(full_name='Степанов Всеволод Михайлович')
    sportsman_1.rank = mc
    sportsman_1.sport_club = sport_club_1

    s_at_tr_1 = DBManager.creator.Sportsmen_at_trainers()
    s_at_tr_1.sportsman = sportsman_1
    s_at_tr_1.trainer = trainer_2

    sportsman_1.trainers.append(s_at_tr_1)

    s_d_s_t_1 = DBManager.creator.Sportsmen_doing_sport_types()
    s_d_s_t_1.sport_type = tx
    s_d_s_t_1.sportsman = sportsman_1

    DBManager.creator.session.add(s_at_tr_1)
    DBManager.creator.session.add(s_d_s_t_1)

    trainer_2.sportsmen.append(s_at_tr_1)

    tx.sportsmen.append(s_d_s_t_1)
    sportsman_1.sport_types.append(s_d_s_t_1)

    DBManager.creator.session.add(sportsman_1)


    sportsman_2 = DBManager.creator.Sportsman(full_name='Алексанов Михаил Алексеевич')
    sportsman_2.rank = mc
    sportsman_2.sport_club = sport_club_2

    s_at_tr_2 = DBManager.creator.Sportsmen_at_trainers()
    s_at_tr_2.sportsman = sportsman_2
    s_at_tr_2.trainer = trainer_1

    sportsman_2.trainers.append(s_at_tr_2)

    s_d_s_t_2 = DBManager.creator.Sportsmen_doing_sport_types()
    s_d_s_t_2.sport_type = football
    s_d_s_t_2.sportsman = sportsman_2

    DBManager.creator.session.add(s_at_tr_2)
    DBManager.creator.session.add(s_d_s_t_2)

    trainer_1.sportsmen.append(s_at_tr_2)

    football.sportsmen.append(s_d_s_t_2)
    sportsman_2.sport_types.append(s_d_s_t_2)

    DBManager.creator.session.add(sportsman_2)



    sportsman_3 = DBManager.creator.Sportsman(full_name='Бабин Савелий Антонович')
    sportsman_3.rank = mc
    sportsman_3.sport_club = sport_club_1

    s_at_tr_3 = DBManager.creator.Sportsmen_at_trainers()
    s_at_tr_3.sportsman = sportsman_3
    s_at_tr_3.trainer = trainer_1

    sportsman_3.trainers.append(s_at_tr_3)

    s_d_s_t_3 = DBManager.creator.Sportsmen_doing_sport_types()
    s_d_s_t_3.sport_type = tx
    s_d_s_t_3.sportsman = sportsman_3

    DBManager.creator.session.add(s_at_tr_3)
    DBManager.creator.session.add(s_d_s_t_3)

    trainer_1.sportsmen.append(s_at_tr_3)

    tx.sportsmen.append(s_d_s_t_3)
    sportsman_3.sport_types.append(s_d_s_t_3)

    DBManager.creator.session.add(sportsman_3)

    #add competitions
    competition = DBManager.creator.Competition(competition_name='Кубок Сибири', competition_date=QDate(2000,1,1).toPyDate())
    competition.organizer = organizer
    competition.facility = facility_1
    competition.sport_type = tx
    DBManager.creator.session.add(competition)

    #add competitors
    competitor = DBManager.creator.Sportsmen_in_competitions()

    competitor.sportsman = sportsman_1
    competitor.competition = competition
    DBManager.creator.session.add(competitor)

    sportsman_1.competitions.append(competitor)
    competition.sportsmen.append(competitor)

    #add awards
    award = DBManager.creator.Award(category='Спарринг до 64 кг 18+')
    award.competition = competition
    award.the_first_place = sportsman_1
    award.the_second_place = sportsman_2
    award.the_third_place = sportsman_3
    DBManager.creator.session.add(award)
    competition.awards.append(award)

    DBManager.creator.session.commit()


if __name__ == '__main__':
    add_all()
