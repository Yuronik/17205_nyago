from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QPushButton, QLabel

from DBManager import DBManager



class RankAddDialog(QDialog):
    rank_name = None
    rank_weight = None

    def __init__(self):
        super().__init__()
        self.init_rank_add_form()

    def init_rank_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Rank name')
        grid_layout.addWidget(label)

        rankNameLine = QLineEdit()
        rankNameLine.textChanged.connect(self.get_rank_name)
        grid_layout.addWidget(rankNameLine)

        label = QLabel()
        label.setText('Rank weight')
        grid_layout.addWidget(label)

        rankWeightLine = QLineEdit()
        rankWeightLine.textChanged.connect(self.get_rank_weight)
        grid_layout.addWidget(rankWeightLine)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def get_rank_name(self, text):
        self.rank_name = text

    def get_rank_weight(self, text):
        self.rank_weight = text

    def button_ok_clicked(self):
        if self.rank_name is not None and "" != self.rank_name and self.rank_weight is not None:
            self.close()
            DBManager.insert_rank(self.rank_name, self.rank_weight)
