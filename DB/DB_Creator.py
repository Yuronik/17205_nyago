from sqlalchemy import create_engine, Column, Integer, String, Date, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import sessionmaker


class DB_creator():
#    engine = create_engine('postgresql://postgres@localhost:5432/new_db', echo=True)
    engine = create_engine('postgresql://postgres:mysecretpassword@localhost:54325/postgres', echo=True)

    base = declarative_base()
    session = sessionmaker(bind=engine)()

    def __init__(self):
        self.base.metadata.create_all(self.engine)

    class Rank(base):
        __tablename__ = 'ranks'
        rank_id = Column('rank_id', Integer, primary_key=True)
        rank_name = Column('rank_name', String)
        weight = Column('weight', Integer)

    class Sport_type(base):
        __tablename__ = 'sport_types'
        sport_type_id = Column('sport_type_id', Integer, primary_key=True)
        type_name = Column('type_name', String)

        sportsmen = relationship('Sportsmen_doing_sport_types', back_populates='sport_type')
        trainers = relationship('Trainers_doing_sport_types', back_populates='sport_type')

    class Organizer(base):
        __tablename__ = 'organizers'
        organizer_id = Column('organizer_id', Integer, primary_key=True)
        organizer_name = Column('organizer_name', String)

    class Facility(base):
        __tablename__ = 'sport_facilities'
        facility_id = Column('facility_id', Integer, primary_key=True)
        facility_name = Column('facility_name', String)
        court_id = Column('court_id', Integer, ForeignKey('court.court_id', ondelete='CASCADE'))
        stadium_id = Column('stadium_id', Integer, ForeignKey('stadium.stadium_id', ondelete='CASCADE'))

        court = relationship('Court', uselist=False)
        stadium = relationship('Stadium', uselist=False)

    class Court(base):
        __tablename__ = 'court'
        court_id = Column('court_id', Integer, primary_key=True)
        coating_type = Column('coating_type', String)

    class Stadium(base):
        __tablename__ = 'stadium'
        stadium_id = Column('stadium_id', Integer, primary_key=True)
        capacity = Column('capacity', Integer)

    class Sport_club(base):
        __tablename__ = 'sport_clubs'
        club_id = Column('club_id', Integer, primary_key=True)
        club_name = Column('club_name', String)

    class Trainer(base):
        __tablename__ = 'trainers'
        trainer_id = Column('trainer_id', Integer, primary_key=True)
        full_name = Column('full_name', String)

        sportsmen = relationship('Sportsmen_at_trainers', back_populates='trainer')
        sport_types = relationship('Trainers_doing_sport_types', back_populates='trainer')

    class Trainers_doing_sport_types(base):
        __tablename__ = 'trainers_doing_sport_types'
        id = Column('id', Integer, primary_key=True)
        trainer_id = Column('trainer_id', Integer, ForeignKey('trainers.trainer_id', ondelete='CASCADE'))
        sport_type_id = Column('sport_type_id', Integer, ForeignKey('sport_types.sport_type_id', ondelete='CASCADE'))

        sport_type = relationship('Sport_type',  back_populates='trainers')
        trainer = relationship('Trainer', back_populates='sport_types')

    class Sportsman(base):
        __tablename__ = 'sportsmen'
        sportsman_id = Column('sportsman_id', Integer, primary_key=True)
        rank_id = Column('rank_id', Integer, ForeignKey('ranks.rank_id', ondelete='CASCADE'))
        sport_club_id = Column('sport_club_id', Integer, ForeignKey('sport_clubs.club_id', ondelete='CASCADE'))
        full_name = Column('full_name', String)

        trainers = relationship('Sportsmen_at_trainers', back_populates='sportsman')
        sport_types = relationship('Sportsmen_doing_sport_types', back_populates='sportsman')

        rank = relationship('Rank', uselist=False)
        sport_club = relationship('Sport_club', uselist=False)
        competitions = relationship('Sportsmen_in_competitions', back_populates='sportsman')


    class Sportsmen_at_trainers(base):
        __tablename__ = 'sportsmen_at_trainers'
        id = Column('id', Integer, primary_key=True)
        trainer_id = Column('trainer_id', Integer, ForeignKey('trainers.trainer_id'))
        sportsman_id = Column('sportsman_id', Integer, ForeignKey('sportsmen.sportsman_id'))

        trainer = relationship('Trainer', back_populates='sportsmen')
        sportsman = relationship('Sportsman', back_populates='trainers')


    class Sportsmen_doing_sport_types(base):
        __tablename__ = 'sportsmen_doing_sport_types'
        id = Column('id', Integer, primary_key=True)
        sport_type_id = Column('sport_type_id', Integer, ForeignKey('sport_types.sport_type_id', ondelete='CASCADE'))
        sportsman_id = Column('sportsman_id', Integer, ForeignKey('sportsmen.sportsman_id', ondelete='CASCADE'))

        sport_type = relationship('Sport_type',  back_populates='sportsmen')
        sportsman = relationship('Sportsman', back_populates='sport_types')


    class Competition(base):
        __tablename__ = 'competitions'
        competition_id = Column('competition_id', Integer, primary_key=True)
        facility_id = Column('facility_id', Integer, ForeignKey('sport_facilities.facility_id', ondelete='CASCADE'))
        organizer_id = Column('organizer_id', Integer, ForeignKey('organizers.organizer_id', ondelete='CASCADE'))
        competition_date = Column('competition_date', Date)
        competition_name = Column('competition_name', String)
        sport_type_id = Column('sport_type_id', Integer, ForeignKey('sport_types.sport_type_id', ondelete='CASCADE'))

        facility = relationship('Facility', uselist=False)
        organizer = relationship('Organizer', uselist=False)
        sport_type = relationship('Sport_type', uselist=False)
        sportsmen = relationship('Sportsmen_in_competitions', back_populates='competition')

        awards = relationship('Award', back_populates='competition')


    class Sportsmen_in_competitions(base):
        __tablename__ = 'sportsmen_in_competitions'
        id = Column('id', Integer, primary_key=True)
        competition_id = Column('competition_id', Integer, ForeignKey('competitions.competition_id', ondelete='CASCADE'))
        sportsman_id = Column('sportsman_id', Integer, ForeignKey('sportsmen.sportsman_id', ondelete='CASCADE'))

        competition = relationship('Competition', back_populates='sportsmen')
        sportsman = relationship('Sportsman', back_populates='competitions')


    class Award(base):
        __tablename__ = 'awards'
        award_id = Column('award_id', Integer, primary_key=True)
        category = Column('category', String)
        competition_id = Column('competition_id', Integer, ForeignKey('competitions.competition_id', ondelete='CASCADE'))
        the_first_place_id = Column('the_first_place_id', Integer, ForeignKey('sportsmen.sportsman_id', ondelete='CASCADE'))
        the_second_place_id = Column('the_second_place_id', Integer, ForeignKey('sportsmen.sportsman_id', ondelete='CASCADE'))
        the_third_place_id = Column('the_third_place_id', Integer, ForeignKey('sportsmen.sportsman_id', ondelete='CASCADE'))

        competition = relationship('Competition', back_populates='awards', uselist=False)
        the_first_place = relationship('Sportsman', uselist=False, foreign_keys=[the_first_place_id])
        the_second_place = relationship('Sportsman', uselist=False, foreign_keys=[the_second_place_id])
        the_third_place = relationship('Sportsman', uselist=False, foreign_keys=[the_third_place_id])

















