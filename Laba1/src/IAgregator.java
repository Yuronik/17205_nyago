import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

public interface IAgregator {
    void addToken(String token);
}
