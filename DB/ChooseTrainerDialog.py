from PyQt5.QtWidgets import QDialog, QComboBox, QGridLayout, QPushButton

from DBManager import DBManager


class ChooseTrainerDialog(QDialog):
    sportsmen_list = None
    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window
        self.init_dialog()

    def init_dialog(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        self.sportsmen_list = QComboBox()
        sportsmen = DBManager.select_sportsman()
        for sportsman in sportsmen:
            self.sportsmen_list.addItem(sportsman.full_name, sportsman)
        grid_layout.addWidget(self.sportsmen_list)

        button_ok = QPushButton("OK")
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def button_ok_clicked(self):
        self.close()
        query = DBManager.show_sportsmen_trainers
        self.main_window.query = query
        self.main_window.params = [self.sportsmen_list.currentData()]
