import java.io.IOException;
import java.io.Writer;
import java.util.*;

public class Aggregator implements IAgregator{
    private Map<String, Integer> allWords = new HashMap<>();
    private int countWords = 0;

    @Override
    public void addToken(String token) {
        this.countWords++;
        this.allWords.merge(token, 1, Integer::sum);
    }
/*
    @Override
    public void csvOutput(Writer writer, ) throws IOException {
        writer.append("Word;Frequency;Frequency %\n");
        for (Map.Entry<String, Integer> entry : sortMap()) {
            writer.append(entry.getKey()).append(";").append(entry.getValue().toString()).append(";").append(Float.toString(100.f* entry.getValue() /  this.countWords)).append("\n");
        }
    }
*/
    public List<Map.Entry<String, Integer>> sortMap() {
        MyComparator comparator = new MyComparator();
        List<Map.Entry<String, Integer>> sortAllWords = new ArrayList<>(allWords.entrySet());
        sortAllWords.sort(comparator);
        return sortAllWords;
    }

    public int getCountWords() {
        return this.countWords;
    }




}


