from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QPushButton, QLabel

from DBManager import DBManager


class SportTypeAddDialog(QDialog):
    type_name = None

    def __init__(self):
        super().__init__()
        self.init_sport_type_add_form()

    def init_sport_type_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Sport type name')
        grid_layout.addWidget(label)

        qLine = QLineEdit()
        qLine.textChanged.connect(self.get_text)
        grid_layout.addWidget(qLine)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def get_text(self, text):
        self.type_name = text

    def button_ok_clicked(self):
        if self.type_name is not None and "" != self.type_name:
            self.close()
            DBManager.insert_sport_type(self.type_name)
#            self.main_window.init_competition_table()

