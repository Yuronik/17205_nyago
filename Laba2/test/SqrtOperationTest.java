package tests.src.test.java;

import org.junit.Assert;
import org.junit.Test;
import ru.my.exception.CalcException;
import ru.my.exception.FewArgException;
import ru.my.operations.SqrtOperation;
import ru.my.source.Context;
import ru.my.source.Parser;


public class SqrtOperationTest {

    @Test
    public void doAction() throws CalcException {
        Context context = new Context();
        SqrtOperation div = new SqrtOperation();
        Parser parser = new Parser();
        String line = "*";
        double value = 4;
        context.push(value);
        div.doAction(context, parser.parseLine(line));
        Assert.assertEquals(Math.sqrt(value), context.pop(), 0.0001);

    }
}