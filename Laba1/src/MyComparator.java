import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

class MyComparator implements Comparator<HashMap.Entry<String, Integer>> {

    @Override
    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
        if (o2.getValue().compareTo(o1.getValue()) == 0)
            return o1.getKey().compareTo(o2.getKey());
        else
            return o2.getValue().compareTo(o1.getValue());

    }
}