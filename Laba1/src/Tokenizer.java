import java.io.*;

public class Tokenizer implements ITokenizer {
    private boolean fileHaveSymbols = false;

    @Override
    public boolean hasNext() {
        return this.fileHaveSymbols;
    }

    @Override
    public String nextToken(Reader reader) throws IOException {
        StringBuilder string = new StringBuilder();
        int symbol = reader.read();
        while (symbol != -1 && (Character.isLetterOrDigit((char)symbol) || string.length() == 0)) {
            if (Character.isLetterOrDigit((char)symbol))
                string.append((char)symbol);
            symbol = reader.read();
        }
        return string.toString();
    }
}
