import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

public interface IDataWriter {
    void csvOutput(Writer writer, List<Map.Entry<String, Integer>> list, int countWords) throws IOException;
}
