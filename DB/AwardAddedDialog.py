from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QComboBox, QPushButton, QLabel
from DBManager import DBManager


class AwardAddedDialog(QDialog):
    category = None
    competition_list = None
    first_place_list = None
    second_place_list = None
    third_place_list = None

    def __init__(self):
        super().__init__()
        self.init_award_add_form()

    def init_award_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        category_label = QLabel()
        category_label.setText('Category')
        grid_layout.addWidget(category_label)

        category_line = QLineEdit()
        category_line.textChanged.connect(self.get_category)
        grid_layout.addWidget(category_line)

        competition_label = QLabel()
        competition_label.setText('Competition')
        grid_layout.addWidget(competition_label)

        self.competition_list = QComboBox()
        competitions = DBManager.select_competitions()
        for competition in competitions:
            self.competition_list.addItem(competition.competition_name, competition)
        grid_layout.addWidget(self.competition_list)

        label = QLabel()
        label.setText('Sportsman at the first place')
        grid_layout.addWidget(label)

        sportsmen = DBManager.select_sportsman()

        self.first_place_list = QComboBox()
        for sportsman in sportsmen:
            self.first_place_list.addItem(sportsman.full_name, sportsman)
        grid_layout.addWidget(self.first_place_list)

        label = QLabel()
        label.setText('Sportsman at the second place')
        grid_layout.addWidget(label)

        self.second_place_list = QComboBox()
        for sportsman in sportsmen:
            self.second_place_list.addItem(sportsman.full_name, sportsman)
        grid_layout.addWidget(self.second_place_list)

        label = QLabel()
        label.setText('Sportsman at the third place')
        grid_layout.addWidget(label)

        self.third_place_list = QComboBox()
        for sportsman in sportsmen:
            self.third_place_list.addItem(sportsman.full_name, sportsman)
        grid_layout.addWidget(self.third_place_list)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def get_category(self, text):
        self.category = text

    def button_ok_clicked(self):
 #       if self.rank_name is not None and "" != self.rank_name and self.rank_weight is not None:
        self.close()
        competition = self.competition_list.currentData()
        first_place = self.first_place_list.currentData()
        second_place = self.second_place_list.currentData()
        third_place = self.third_place_list.currentData()
        DBManager.insert_award(self.category, competition, first_place, second_place, third_place)

