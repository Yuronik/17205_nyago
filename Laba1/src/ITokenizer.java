import java.io.IOException;
import java.io.Reader;

public interface ITokenizer {
    boolean hasNext();
    String nextToken(Reader reader) throws IOException;
}
