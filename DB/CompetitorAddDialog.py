from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QPushButton, QComboBox, QLabel

from DBManager import DBManager


class CompetitorAddDialog(QDialog):
    competition_list = None
    sportsmen_list = None

    def __init__(self):
        super().__init__()
        self.init_competitor_add_form()

    def init_competitor_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Sportsman')
        grid_layout.addWidget(label)

        self.sportsmen_list = QComboBox()
        sportsmen = DBManager.select_sportsman()
        for sportsman in sportsmen:
            self.sportsmen_list.addItem(sportsman.full_name, sportsman)
        grid_layout.addWidget(self.sportsmen_list)

        label = QLabel()
        label.setText('Competition')
        grid_layout.addWidget(label)

        self.competition_list = QComboBox()
        competitions = DBManager.select_competitions()
        for competition in competitions:
            self.competition_list.addItem(competition.competition_name, competition)
        grid_layout.addWidget(self.competition_list)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def button_ok_clicked(self):
#        if self.full_name is not None and "" != self.full_name:
            self.close()
            competition = self.competition_list.currentData()
            sportsman = self.sportsmen_list.currentData()
            DBManager.insert_competitor(competition, sportsman)

