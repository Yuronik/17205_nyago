from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QPushButton, QComboBox, QLabel

from DBManager import DBManager


class TrainerAddDialog(QDialog):
    full_name = None
    sport_type_list = None

    def __init__(self):
        super().__init__()
        self.init_trainer_add_form()

    def init_trainer_add_form(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        label = QLabel()
        label.setText('Trainer name')
        grid_layout.addWidget(label)

        qLine = QLineEdit()
        qLine.textChanged.connect(self.get_text)
        grid_layout.addWidget(qLine)

        label = QLabel()
        label.setText('Sport type')
        grid_layout.addWidget(label)

        self.sport_type_list = QComboBox()
        sport_types = DBManager.select_sport_types()
        for sport_type in sport_types:
            self.sport_type_list.addItem(sport_type.type_name, sport_type)
        grid_layout.addWidget(self.sport_type_list)

        button_ok = QPushButton("OK", self)
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def get_text(self, text):
        self.full_name = text

    def button_ok_clicked(self):
            self.close()
            name = self.full_name
            sport_type = self.sport_type_list.currentData()
            DBManager.insert_trainer(name, sport_type)

