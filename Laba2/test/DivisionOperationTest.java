package tests.src.test.java;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;
import ru.my.exception.CalcException;
import ru.my.exception.DivisionByZeroException;
import ru.my.operations.DivisionOperation;
import ru.my.source.Context;
import ru.my.source.Parser;

public class DivisionOperationTest {
    @Test
    public void doAction() throws CalcException {
        Context context = new Context();
        DivisionOperation div = new DivisionOperation();
        Parser parser = new Parser();
        String line = "/";
        double value1 = 1;
        double value2 = 2;
        context.push(value1);
        context.push(value2);
        div.doAction(context, parser.parseLine(line));
        Assert.assertEquals(value2/value1, context.pop(), 0.0001);
    }

}