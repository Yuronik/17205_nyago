from PyQt5.QtWidgets import QDialog, QComboBox, QGridLayout, QPushButton

from DBManager import DBManager


class ChooseSportTypeDialog(QDialog):
    sport_type_list = None
    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window
        self.init_dialog()

    def init_dialog(self):
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        self.sport_type_list = QComboBox()
        sport_types = DBManager.select_sport_types()
        for sport_type in sport_types:
            self.sport_type_list.addItem(sport_type.type_name, sport_type)
        grid_layout.addWidget(self.sport_type_list)

        button_ok = QPushButton("OK")
        button_ok.clicked.connect(self.button_ok_clicked)
        grid_layout.addWidget(button_ok)

        self.exec()

    def button_ok_clicked(self):
        self.close()
        query = DBManager.show_trainer_by_sport_type
        self.main_window.query = query
        self.main_window.params = [self.sport_type_list.currentData()]
